
import java.util.ArrayList;

import tanling.matplot_4j.d3d.base.pub.Point3D;
import tanling.matplot_4j.d3d.facade.MatPlot3DMgr;

public class DotsDemo {

	public static void main(String[] args) throws Exception {

		MatPlot3DMgr mgr = new MatPlot3DMgr();

		mgr.setDataInputType(MatPlot3DMgr.DATA_TYPE_DOTS);

		//*************************************************************//
		//Add your data here
		
	    mgr.addData("Item 1", new ArrayList<Point3D>());
	    mgr.addData("Item 2", new ArrayList<Point3D>());
	    mgr.addData("Item 3", new ArrayList<Point3D>());
		//.................
	    
		mgr.setScaleX(1.2);
		mgr.setScaleY(1.2);
		mgr.setScaleZ(1.2);
		
		mgr.setSeeta(0.6);
		mgr.setBeita(1.0);
		
		mgr.setTitle("Demo : MNIST 数据集机器学习分类问题数据 3维 空间分布");

		mgr.getProcessor().setCoordinateSysShowType(mgr.getProcessor().COORDINATE_SYS_ALWAYS_FURTHER);

		mgr.show();
	}

}
